package ictgradschool.industry.lab16.ex02.model;


import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Iterator;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

    /**********************************************************************
     * YOUR CODE HERE
     */

    private Course course;

    public CourseAdapter(Course course) {
//		this.table = table;
        this.course = course;
    }

    @Override
    public void courseHasChanged(Course course) {
        Iterator<StudentResult> studentResults = course.iterator();
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return course.size();
    }

    @Override
    public int getColumnCount() {

        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        StudentResult student = course.getResultAt(rowIndex);
        switch (columnIndex) {
            case 0:
                return student._studentID;
            case 1:
                return student._studentSurname;
            case 2:
                return student._studentForename;
            case 3:
                return student.getAssessmentElement(StudentResult.AssessmentElement.Exam);
            case 4:
                return student.getAssessmentElement(StudentResult.AssessmentElement.Test);
            case 5:
                return student.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
            case 6:
                return student.getAssessmentElement(StudentResult.AssessmentElement.Overall);
            default:
                return null;
        }

    }

    @Override
    public String getColumnName(int column) {
        String result = "";
        if (column == 0) {
            result = "StudentID";
        } else if (column == 1) {
            result = "Surname";
        } else if (column == 2) {
            result = "Forename";
        } else if (column == 3) {
            result = "Exam";
        } else if (column == 4) {
            result = "Test";
        } else if (column == 5) {
            result = "Assignment";
        } else if (column == 6) {
            result = "Overall";
        }
        return result;
    }

}